MikuMikuDance Model import into Unreal Engine 5
===
I have had some free time on my hands lately and have been meaning to get more into UnrealEngine. Normally I focus on procedural work  but figured I should try and properly learn some of the more relevant parts of Unreal as well like how to import and animate models. Unfortunately I am not a 3D animator / modeler by trade and I have not been able to find freely available models + skeletal rigs avaiable(though they're probably out there and I just simply don't know where to look). 

Fortunately, Cover Corp very kindly provides rigged [3D models](https://www.mmd.hololive.tv/) for some of the JP talents on their website. However, for whatever reason they are provided in a format that only seems to be compatible with 2 programs, [MikuMikuDance](https://learnmmd.com/) and PMX editor(see link below); they are not compatible with more commonplace software like Blender, Maya, etc.

Luckily I am not the only one that has thought of porting MikuMikuDance models to UE! I've only managed to find one guide for UE 4 and haven't seen anyone put up actual assets so I thought I would put this together.

This has been tested with version 5.1 downloaded from Github. I am using the `release` branch so this "should" work with the version you get from the Epic Launcher as well if you're unfamiliar with how to use Git. 

Also note that you should be able to follow these instructions with any MMD/PMX model. And of course, models generated from the tools described here can be used anywhere, not just Unreal Engine. 

Included assets 
=== 
* Assets for [Choco sensei](https://twitter.com/yuzukichococh) are included in this repository. Note that this, and I believe the other models, do not include any animation(though there appear to be some tail physics applied to Choco's model but I'm not sure how to translate that to UE yet).  
* [Watame](https://twitter.com/tsunomakiwatame) has been added as well. However due to space constraints, the Blender file is not included. The textures are also not included, you will also need to make sure you download them from NicoNico prior to use. 

Software Needed
====
* [Blender](https://www.blender.org/download/)
* [MMD Tools Blender Plugin](https://github.com/UuuNyaa/blender_mmd_tools) - This is needed in order to import the `.pmx` model into Blender
* [PMXEditor](https://www.deviantart.com/inochi-pm/art/PmxEditor-vr-0254f-English-Version-v2-0-766313588)


Importing things into UE5
====
The following steps described are all pretty much from [this guide](https://www.deviantart.com/thehoodieguy02/journal/How-to-make-Unreal-Engine-4-your-new-MMD-Pt-2-892643542) from [@thehoodieguy02](https://www.deviantart.com/thehoodieguy02) on Deviant Art. For the most part, you should be covered by the guide. 

__Some notes though__

* UE5 seems to have decent support for Japanese now, so I don't think you have to technically rename anything unless you want to. MMD tools also has an option to convert Japanese to English and will do it's best to make what translations that it can.
* I cannot find the "Apply to selection" option in the most recent version of Blender so it might be a bit of a pain to turn off "Deform" on the bone elements(FYI I think I forgot a few bones on this model but it shouldn't be hard to go back and re-edit.)
* It seems like Cover built the models with texture atlases whereas the guide appears to be basing things off of a model that has a mixture of atlases and separate textures; this is where PMXEditor comes in, you can use it to reference which material goes with which body part. You should create a base Material for each atlas and build material instances off of those to texture everything. You should already be getting the proper UVs from the mesh so when building the material, you just need the texture coordinate node and a sampler node set to an atlas.  
* PMXEditor will also provide other information like material / rendering settings you can try using.
* When importing the FBX file, it might be worthwhile to just let UE try and recreate the materials first so the associations to the respective body parts will be created as well. Then you just have to replace the associated material with your material instance and you won't have to manually make the associations yourself.
* In the sample assets, I skipped fully setting up the material so you'll have to make your own adjustments according to your scene.
  * Note also that I've kept the Japanese in some of the assets, assets will generally folow the format `<japanese name>_<english name>`


Cover Corp Licence 
=== 
Cover does have some reasonable conditions for using the models which you can find in the readme file included with each model but for reference: 
*(translated with DeepL as my Japanese is not very good)*


Terms of Use

- The rights to the Hololive officially distributed models for MikuMikuDance (MMD) (hereinafter referred to as "the Contents"), as well as the copyright and intellectual property rights related to the Contents belong to Cover Corp.

- Please comply with the Hololive Secondary Creation License Agreement (https://www.hololive.tv/terms) applicable to the Content.

- Cover Inc. shall not be liable for any damage, disadvantage, or other trouble incurred by the user as a result of using the Contents.

- When publishing or distributing derivative works using the Contents, please indicate the license notice © 2019 cover corp. so that viewers, consumers, etc. can clearly recognize it.

- Please contact Cover Corp. (https://www.hololive.tv/contact) in advance for commercial use or any other use for which compensation will be charged.

- Please do not modify or use the contents in such a way as to cause trouble to others, such as by transplanting parts from other 3D models without permission. Please modify and distribute the data in accordance with the Terms of Use and Prohibitions.

- The Terms of Use and Prohibitions are subject to revision without notice. If the Terms of Use or Prohibitions are revised, the latest version shall take precedence. Please use the site in accordance with the latest Terms of Use and Prohibitions.



Prohibitions

Reproducing or redistributing data from the Contents.

2. Transplanting a part of the model or using it as a material to create another model from the contents of this site.

3.Transplanting parts from a model for which transplanting parts is prohibited, or otherwise modifying or using the model in such a way as to cause inconvenience to others.

4. Use for acts or purposes that are offensive to public order and morals, violent expressions, antisocial acts or purposes, or specific beliefs, religions, or political statements.

5. Using the Contents to offend, discriminate against or injure others.

6. Infringing the intellectual property rights or any other rights or reputation of any third party.

7. Use the Contents in a manner that diminishes the value or dignity of the characters associated with the Contents.

8. Use the Contents in a manner that does not conform to the wishes of the talent in the Contents.

9. To use the Content in a manner that may mislead people into believing that the Content is an official product of Cover, Inc.

10. To use the Contents for any other activities that Cover Inc. deems inappropriate.

